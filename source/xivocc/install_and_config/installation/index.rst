############
Installation
############

.. toctree::
   :maxdepth: 2

   installation
   manual_configuration
   desktop_assistant

