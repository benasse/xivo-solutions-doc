*****************
Desktop Assistant
*****************

On first launch the application will display the settings page and ask for the xucmgt application address. 

Navigation
==========

.. figure:: navigation.png
   :scale: 100%


The top menu allows you to navigate either to the application or to the settings page. If you did not enter any setting, the application will redirect you to the settings page.

Settings
========

.. figure:: settings.png
   :scale: 100%

This page allows you to specify the protocol and address of the xucmgt application.

* Check "Secure" if you use "https" protocol to connect to the xucmgt application or check "Unsecure" otherwise.
* Enter the xucmgt application host address and port.


Update
======

On Windows, the application will check at startup for a new version of the application and offer to upgrade if one is available.

.. figure:: update.png
   :scale: 100%


On Debian, the update relies on the package manager behaviour. However you can check for any update by issuing the following commands:

.. code-block:: bash

                sudo apt-get update
                apt-cache policy xivo-desktop-assistant


Options
=======

The Desktop Assistant can be started with following options:

* -d to enable debug menu items
* --ignore-certificate-errors to disable certificate verification, this option is meant **only** for test purposes. You can use it with self-signed certificates.


WebRTC integration
==================

The *Desktop Assistant* can be used by users with WebRTC configuration, without physical phone.

For configuration and requirements, see :ref:`webrtc_requirements`.

